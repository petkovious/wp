package com.example.demo.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Data
@Table(name = "store" ,schema = "public")
public class Store {


    public Store(String name) {
        this.store_name = name;
    }

    @Id
    private String store_name;

    public Store() {

    }
}
