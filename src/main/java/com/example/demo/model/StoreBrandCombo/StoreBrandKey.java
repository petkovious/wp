package com.example.demo.model.StoreBrandCombo;

import com.example.demo.model.Brand;
import com.example.demo.model.Store;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StoreBrandKey implements Serializable {

    @ManyToOne
    @JoinColumn(name="brand_name")
    public Brand brand;

    @ManyToOne
    @JoinColumn(name="store_name")
    public Store store;


}
