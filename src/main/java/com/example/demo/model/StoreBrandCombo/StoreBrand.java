package com.example.demo.model.StoreBrandCombo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="store_brand")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreBrand {

    @EmbeddedId
    StoreBrandKey storeBrandKey;

}
