package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "shirt" ,schema = "public")
public class Shirt {

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Id
    private String shirt_name;

    private Long supply;

    private String description;

    @ManyToOne
    @JoinColumn(name="store_name")
    private Store store;

    public Shirt() {
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Shirt(String name, Long supply, String description, Store store) {
        this.shirt_name = name;
        this.supply = supply;
        this.description = description;
        this.store = store;
    }

    public String getName() {
        return shirt_name;
    }

    public void setName(String name) {
        this.shirt_name = name;
    }

    public Long getSupply() {
        return supply;
    }

    public void setSupply(Long supply) {
        this.supply = supply;
    }
}
