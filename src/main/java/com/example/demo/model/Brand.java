package com.example.demo.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="brand", schema = "public")
@Data
public class Brand {

    @Id
    private String brand_name;

    public Brand(String name){
        this.brand_name = name;
    }

    public Brand() {

    }
}
