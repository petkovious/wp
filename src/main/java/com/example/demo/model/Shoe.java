package com.example.demo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@Table(name = "shoe" ,schema = "public")
public class Shoe {

    public Shoe() {
    }

    public Shoe(String name, String description,Store store, Long supply) {
        this.store = store;
        this.shoe_name = name;
        this.description = description;
        this.supply = supply;
    }

    @Id
    private String shoe_name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "store_name")
    private Store store;

    private Long supply;


    public String getShoe_name() {
        return shoe_name;
    }

    public void setShoe_name(String name) {
        this.shoe_name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Long getSupply() {
        return supply;
    }

    public void setSupply(Long supply) {
        this.supply = supply;
    }
}





