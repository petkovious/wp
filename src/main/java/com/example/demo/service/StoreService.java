package com.example.demo.service;

import com.example.demo.model.Store;

import java.util.List;

public interface StoreService {
    List<Store> listAll();

    Store findById(String name);

    Store create(String name);

    Store update(String oldName, String newName);

    Store delete(Long id);
}
