package com.example.demo.service;

import com.example.demo.model.Brand;

import java.util.List;

public interface BrandService {
    List<Brand> listAll();

    Brand findById(String name);

    Brand create(String name);

    void delete(String name);
}
