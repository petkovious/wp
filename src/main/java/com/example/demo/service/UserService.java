package com.example.demo.service;


import com.example.demo.model.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UserService {

    User findById(Long id);

    List<User> listAll();

    User create(String username, String password, String role);


    UserDetails loadUserByUsername(String username);
}
