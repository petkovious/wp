package com.example.demo.service.impl;

import com.example.demo.model.Shoe;
import com.example.demo.model.Store;
import com.example.demo.model.exceptions.InvalidShoeIdException;
import com.example.demo.model.exceptions.InvalidStoreIdException;
import com.example.demo.repositorium.ShoeRepository;
import com.example.demo.repositorium.StoreRepository;
import com.example.demo.service.ShoeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShoeServiceImpl implements ShoeService {

    private final ShoeRepository shoeRepository;
    private final StoreRepository storeRepository;

    public ShoeServiceImpl(ShoeRepository shoeRepository, StoreRepository storeRepository) {
        this.shoeRepository = shoeRepository;
        this.storeRepository = storeRepository;
    }

    @Override
    public List<Shoe> listAll() {
        return shoeRepository.findAll();
    }

    @Override
    public Shoe findById(String name) {
        return shoeRepository.findById(name).orElseThrow(InvalidShoeIdException::new);
    }


    @Override
    public Shoe create(String title, String description, Long supply, String store_name) {
        Store store = storeRepository.findById(store_name).orElse(storeRepository.save(new Store(store_name)));
        return shoeRepository.save(new Shoe(title, description, store, supply));
    }

    @Override
    public Shoe update(String id, String description, Long supply, String store_name) {
        Store store = storeRepository.findById(store_name).orElse(storeRepository.save(new Store(store_name)));

        Shoe shoe = shoeRepository.findById(id).orElseThrow(InvalidShoeIdException::new);
        shoe.setDescription(description);
        shoe.setSupply(supply);
        shoe.setStore(store);
        return shoeRepository.save(shoe);
    }

    @Override
    public Shoe delete(String id) {
        Optional<Shoe> shoe = shoeRepository.findById(id);
        if (shoe.isPresent()) {
            shoeRepository.deleteById(id);
            return shoe.get();
        } else
            throw new InvalidShoeIdException();
    }

    @Override
    public Shoe setSupply(String id) {
        Shoe shoe = shoeRepository.findById(id).orElseThrow(InvalidShoeIdException::new);
        shoe.setSupply(0L);
        return shoeRepository.save(shoe);
    }

    @Override
    public List<Shoe> filter(Long assigneeId, Integer lessThanDayBeforeDueDate) {
//        User user = userRepository.findById(assigneeId).orElseThrow(InvalidUserIdException::new);
//        Optional<List<Shoe>> tmpList = shoeRepository.findAllByAssignees(user);
//        if (tmpList.isPresent()) {
//            List<Shoe> tmpListGet = tmpList.get();
//            LocalDate localDate = LocalDate.now().plusDays(lessThanDayBeforeDueDate);
//            tmpListGet = tmpList.get().stream().filter(shoe -> {
//                return shoe.getDueDate().isBefore(localDate);
//            }).collect(Collectors.toList());
//            return tmpListGet;
//        } else {
//            List<Shoe> fullList = shoeRepository.findAll();
//            LocalDate localDate = LocalDate.now().plusDays(lessThanDayBeforeDueDate);
//            fullList = tmpList.get().stream().filter(shoe -> {
//                return shoe.getDueDate().isBefore(localDate);
//            }).collect(Collectors.toList());
//            return fullList;
//        }
        return this.shoeRepository.findAll();
    }
}
