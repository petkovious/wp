package com.example.demo.service.impl;

import com.example.demo.model.Brand;
import com.example.demo.model.exceptions.InvalidBrandException;
import com.example.demo.repositorium.BrandRepository;
import com.example.demo.service.BrandService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;

    public BrandServiceImpl(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Override
    public List<Brand> listAll() {
        return brandRepository.findAll();
    }

    @Override
    public Brand findById(String name) {
        return brandRepository.findById(name).orElseThrow(InvalidBrandException::new);
    }

    @Override
    public Brand create(String name) {
        return brandRepository.save(new Brand(name));
    }

    @Override
    public void delete(String name) {
        brandRepository.deleteById(name);
    }
}
