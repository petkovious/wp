package com.example.demo.service.impl;

import com.example.demo.model.Shirt;
import com.example.demo.model.Store;
import com.example.demo.model.exceptions.InvalidShirtException;
import com.example.demo.model.exceptions.InvalidShoeIdException;
import com.example.demo.repositorium.ShirtRepository;
import com.example.demo.repositorium.StoreRepository;
import com.example.demo.service.ShirtService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShirtServiceImpl implements ShirtService {

    private final ShirtRepository shirtRepository;
    private final StoreRepository storeRepository;

    public ShirtServiceImpl(ShirtRepository shirtRepository, StoreRepository storeRepository) {
        this.shirtRepository = shirtRepository;
        this.storeRepository = storeRepository;
    }

    @Override
    public List<Shirt> listAll() {
        return shirtRepository.findAll();
    }

    @Override
    public Shirt findById(String name) {
        return shirtRepository.findById(name).orElseThrow(InvalidShirtException::new);
    }

    @Override
    public Shirt create(String name,String description, Long supply, String store_name) {
        Store store = storeRepository.findById(store_name).orElse(storeRepository.save(new Store(store_name)));
        return shirtRepository.save(new Shirt(name, supply, description, store));
    }


    @Override
    public void delete(String name) {
        shirtRepository.deleteById(name);
    }

    @Override
    public Shirt update(String id, String description, Long supply, String store_name) {
        Store store = storeRepository.findById(store_name).orElse(storeRepository.save(new Store(store_name)));
        Shirt shirt = shirtRepository.findById(id).orElseThrow(InvalidShoeIdException::new);
        shirt.setDescription(description);
        shirt.setSupply(supply);
        shirt.setStore(store);
        return shirtRepository.save(shirt);
    }
}
