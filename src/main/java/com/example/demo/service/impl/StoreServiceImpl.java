package com.example.demo.service.impl;

import com.example.demo.model.Brand;
import com.example.demo.model.Store;
import com.example.demo.repositorium.BrandRepository;
import com.example.demo.repositorium.StoreRepository;
import com.example.demo.service.StoreService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {


    private final StoreRepository storeRepository;
    private final BrandRepository brandRepository;

    public StoreServiceImpl(StoreRepository storeRepository, BrandRepository brandRepository) {
        this.storeRepository = storeRepository;
        this.brandRepository = brandRepository;
    }

    @Override
    public List<Store> listAll() {
        return null;
    }

    @Override
    public Store findById(String name) {
        return null;
    }

    @Override
    public Store create(String name) {
        return storeRepository.save(new Store(name));
    }

    @Override
    public Store update(String oldName, String newName) {
        return null;
    }

    @Override
    public Store delete(Long id) {
        return null;
    }
}
