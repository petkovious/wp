package com.example.demo.service;

import com.example.demo.model.Shirt;
import com.example.demo.model.Shoe;
import com.example.demo.model.Store;


import java.util.List;

public interface ShirtService {
    List<Shirt> listAll();

    Shirt findById(String name);

    Shirt create(String name, String description, Long supply, String store_name);

    void delete(String name);

    Shirt update(String id, String description, Long supply, String store_name);

}
