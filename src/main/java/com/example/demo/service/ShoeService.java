package com.example.demo.service;


import com.example.demo.model.Shoe;

import java.util.List;

public interface ShoeService {


    List<Shoe> listAll();

    Shoe findById(String name);

    Shoe create(String title, String description, Long supply, String store_name);

    Shoe update(String id, String description, Long supply, String store_name);

    Shoe delete(String id);

    Shoe setSupply(String id);

    List<Shoe> filter(Long assigneeId, Integer lessThanDayBeforeDueDate);
}
