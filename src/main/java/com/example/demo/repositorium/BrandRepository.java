package com.example.demo.repositorium;

import com.example.demo.model.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;

@Repository
public interface BrandRepository extends JpaRepository<Brand, String> {
}
