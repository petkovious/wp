package com.example.demo.repositorium;

import com.example.demo.model.StoreBrandCombo.StoreBrand;
import com.example.demo.model.StoreBrandCombo.StoreBrandKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreBrandRepository extends JpaRepository<StoreBrand, StoreBrandKey> {

}
