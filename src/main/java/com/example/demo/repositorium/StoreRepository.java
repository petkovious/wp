package com.example.demo.repositorium;

import com.example.demo.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<Store,String> {
}
