package com.example.demo.WEB;

import com.example.demo.model.Shirt;
import com.example.demo.model.Shoe;
import com.example.demo.model.Store;
import com.example.demo.service.ShirtService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/t-shirt")
public class ShirtsController {

    private final ShirtService shirtService;

    public ShirtsController(ShirtService shirtService) {
        this.shirtService = shirtService;
    }

    @GetMapping
    public String getViewHome(Model model) {

        List<Shirt> shirt = shirtService.listAll();
        model.addAttribute("shirt",shirt);
        return "shirts";
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable String id) {
        this.shirtService.delete(id);
        return "redirect:/t-shirt";
    }


    @GetMapping("/{id}/edit")
    public String showEdit(@PathVariable String id, Model model) {
        Shirt shirt = shirtService.findById(id);
        model.addAttribute("shirt", shirt);
        return "add-shirt";
    }

    @GetMapping("/add-form")
    public String shoeAdd(Model model) {
//        List<Shirt> shirt = this.shirtService.listAll();
//        List<Store> store = this.store.listAll();
//        model.addAttribute("shirt",shirt);
//        model.addAttribute("store",store);
        return "add-shirt";
    }

    @PostMapping("/{id}")
    public String update(@PathVariable String id,
                         @RequestParam String description,
                         @RequestParam Long  supply,
                         @RequestParam String store_name) {
        this.shirtService.update(id, description, supply, store_name);
        return "redirect:/t-shirt";
    }

    @PostMapping("/")
    public String saveProjection(
            @RequestParam(required = false) String shirt_name,
            @RequestParam String description,
            @RequestParam Long supply,
            @RequestParam String store_name
    ) {
        this.shirtService.create(shirt_name, description, supply, store_name);
        return "redirect:/shoe";
    }
}
