package com.example.demo.WEB;


import com.example.demo.model.Shoe;
import com.example.demo.model.Store;
import com.example.demo.service.ShoeService;
import com.example.demo.service.StoreService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
public class HomeController {

    private final ShoeService shoeService;
    private final StoreService storeService;

    public HomeController(ShoeService shoeService, StoreService storeService) {
        this.shoeService = shoeService;
        this.storeService = storeService;
    }

    @GetMapping({"/","/shoe"})
    public String getViewHome(Model model) {
        List<Shoe> shoe = shoeService.listAll();
        model.addAttribute("shoe",shoe);
        return "home";
    }

    @PostMapping("/shoe/{id}/delete")
    public String delete(@PathVariable String id) {
        this.shoeService.delete(id);
        return "redirect:/shoe";
    }

    @PostMapping("/shoe")
    public String saveProjection(
            @RequestParam(required = false) String shoe_name,
            @RequestParam String description,
            @RequestParam Long supply,
            @RequestParam String store_name

            ) {
        this.shoeService.create(shoe_name, description, supply, store_name);
        return "redirect:/shoe";
    }

    @GetMapping("/shoe/{id}/edit")
    public String showEdit(@PathVariable String id, Model model) {
        Shoe shoe = shoeService.findById(id);
        model.addAttribute("shoe", shoe);
        return "add-shoe";
    }

    @GetMapping("/shoe/add-form")
    public String shoeAdd(Model model) {
//        List<Shoe> shoe = this.shoeService.listAll();
//        List<Store> store = this.storeService.listAll();
//        model.addAttribute("shoe",shoe);
//        model.addAttribute("store",store);
        return "add-shoe";
    }

    @PostMapping("/shoe/{id}")
    public String update(@PathVariable String id,
                         @RequestParam String description,
                         @RequestParam Long  supply,
                         @RequestParam String store_name) {
        this.shoeService.update(id, description, supply, store_name);
        return "redirect:/shoe";
    }



}
