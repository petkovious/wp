insert into users (id, username, password, role)
values (1, 'user1', 'pass1', 'ADMIN'), 
(2, 'user2', 'pass2', 'CLIENT');

insert into brand (brand_name)
values ('nike'),('vans');

insert into v(store_name)
values ('waikiki'), ('zara');

insert into shirt(shirt_name, description, store_name, supply)
values ('shirt1','cotton' , 'waikiki', 5), 
('shirt2','cotton' , 'waikiki', 5), 
('shirt3','cotton' , 'waikiki', 5),
('shirt4','cotton' , 'zara', 5), 
('shirt5','cotton' , 'zara', 5),
('shirt6','cotton' , 'zara', 5);

insert into shoe(shoe_name, description, store_name, supply)
values ('shoe1', 'cotton', 'waikiki', 5), 
('shoe2', 'cotton', 'waikiki', 5), 
('shoe3', 'cotton', 'waikiki', 5), 
('shoe4', 'cotton', 'zara', 5), 
('shoe5', 'cotton', 'zara', 5);

insert into store_brand(store_name, brand_name)
values ('waikiki', 'nike'), ('zara', 'nike'), ('zara', 'vans');
