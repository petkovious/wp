drop schema if exists public cascade;
create schema public;

drop table if exists brand cascade;
drop table if exists shoes cascade;
drop table if exists shirts cascade;
drop table if exists store cascade;
drop table if exists store_brand cascade;
drop table if exists users cascade;

create table brand(
    brand_name varchar(100) primary key
);


create table store(
    store_name varchar(100) primary key
);

create table shirt(
    shirt_name varchar(100) primary key,
    store_name varchar(100),
    supply bigint,
    description varchar(100) not null,
    constraint fk_shirt_store foreign key (store_name) references store(store_name)
);

create table shoe(
    shoe_name varchar(100) primary key,
    description varchar(100) not null,
    store_name varchar(100),
    supply bigint,
    constraint fk_shoe_store foreign key (store_name) references store(store_name)
);

create table store_brand(

    store_name varchar(100),
    brand_name varchar(100),
    
    constraint pk_is_genre primary key (store_name,brand_name),
    constraint fk_is_movie foreign key (store_name) references store(store_name),
    constraint fk_controles_genre foreign key (brand_name) references brand(brand_name)
);

create table users(
    id serial primary key,
    username varchar(100) not null,
    password varchar(100) not null,
    role varchar(100) not null
);
